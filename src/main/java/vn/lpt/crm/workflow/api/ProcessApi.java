package vn.lpt.crm.workflow.api;

import io.swagger.annotations.*;
import org.activiti.api.process.model.payloads.StartProcessPayload;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import vn.lpt.crm.workflow.model.ProccessRequest;
import vn.lpt.crm.workflow.model.Response;

import javax.validation.Valid;

@Api(value = "processes")
public interface ProcessApi {

  @ApiOperation(
      value = "get process definitions",
      nickname = "getProcessDefinition",
      response = Response.class,
      tags = {
        "processes",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "success", response = Response.class),
      })
  @GetMapping(value = "/processes/definitions", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<Response> getProcessDefinition();

  @ApiOperation(
      value = "start process",
      nickname = "startProcess",
      response = Response.class,
      tags = {
        "processes",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "success", response = Response.class),
      })
  @PostMapping(
      value = "/processes/instances",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<Response> startProcess(@ApiParam @Valid @RequestBody StartProcessPayload payload);

  @ApiOperation(
      value = "inprogress process",
      nickname = "inprogress",
      response = Response.class,
      tags = {
        "processes",
      })
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "success", response = Response.class),
      })
  @PostMapping(
      value = "/processes/inprogress",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<Response> proccess(@ApiParam @Valid @RequestBody ProccessRequest proccessRequest);
}
