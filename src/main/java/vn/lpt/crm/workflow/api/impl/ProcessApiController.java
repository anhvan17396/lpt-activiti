package vn.lpt.crm.workflow.api.impl;

import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.payloads.StartProcessPayload;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.lpt.crm.workflow.api.ProcessApi;
import vn.lpt.crm.workflow.model.ProccessRequest;
import vn.lpt.crm.workflow.model.Response;
import vn.lpt.crm.workflow.model.TestUpload;
import vn.lpt.crm.workflow.services.ProcessService;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/v1")
public class ProcessApiController implements ProcessApi {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProcessApiController.class);
  private final ProcessService processService;
  @Autowired HistoryService historyService;
  @Autowired RepositoryService repositoryService;
  @Autowired private RuntimeService runtimeService;
  @Autowired private TaskService taskService;

  public ProcessApiController(ProcessService processService) {
    this.processService = processService;
  }

  @Override
  public ResponseEntity<Response> getProcessDefinition() {
    return null;
  }

  // Start process
  @Override
  public ResponseEntity<Response> startProcess(@Valid StartProcessPayload payload) {
    ProcessInstance processInstance = processService.startProcess(payload);
    System.out.println(processInstance.getProcessDefinitionId());
    List<ProcessInstance> list = Collections.singletonList(processInstance);
    Response response = Response.builder().data(list).build();
    return ResponseEntity.ok(response);
  }

  @Override
  public ResponseEntity<Response> proccess(@Valid ProccessRequest proccessRequest) {
    try {
      List<ProccessRequest> lst = new ArrayList<ProccessRequest>();
      Response response = new Response();
      List<Task> tasks = new ArrayList<Task>();
      if (proccessRequest.getObjectId() == null || proccessRequest.getObjectId().isEmpty()) {
        tasks =
            taskService
                .createTaskQuery()
                .taskVariableValueEquals("id", proccessRequest.getLeadId())
                .taskVariableValueEquals("accNo", proccessRequest.getAccNo())
                .taskVariableValueEquals("name", proccessRequest.getName())
                .taskVariableValueEquals("phoneMobile", proccessRequest.getPhoneMobile())
                .orderByTaskName()
                .asc()
                .list();
      } else {
        tasks =
            taskService
                .createTaskQuery()
                .processInstanceId(proccessRequest.getObjectId())
                .orderByTaskName()
                .asc()
                .list();
      }
      // TODO Auto-generated method stub
      if (proccessRequest == null || tasks == null || tasks.size() == 0) {
        System.out.print("\nProcess/tasks is null !\n");
        return null;
      }
      if (tasks.size() > 1) {
        System.out.print("\nHave more than one task!\n");
        return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(null);
      }
      for (Task task : tasks) {
        proccessRequest.setTaskId(task.getId());
        lst.add(proccessRequest);
        //				taskService.setv
        //				taskService.complete(proccessRequest.getTaskId(), proccessRequest.getCondition());
        System.out.print("\nTask: " + task.getId() + "\n");
      }
      // lay buoc tiep theo va cac condition tiep theo

      // tra ra response
      response.setData(lst);
      return ResponseEntity.ok(response);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }



  @PostMapping(value = "/processes/testResponse", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> testResponse(@RequestBody List<Object> lstObject) {
    TestUpload data = new TestUpload();
    data.setData(lstObject);
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("data", data);
    org.activiti.engine.runtime.ProcessInstance processInstance =
        runtimeService.startProcessInstanceByKey("myProcess", map);
    return new ResponseEntity<Object>(null, HttpStatus.OK);
  }
}
