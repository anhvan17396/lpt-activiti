package vn.lpt.crm.workflow.model;

import io.swagger.annotations.ApiModel;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class Error {
  private String code;
  private String description;
}
