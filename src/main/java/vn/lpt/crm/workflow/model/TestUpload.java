package vn.lpt.crm.workflow.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TestUpload {

  private List<Object> data;
}
