package vn.lpt.crm.workflow.model;

import io.swagger.annotations.ApiModel;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class Response {
  private List<?> data;
  private List<Error> errors;
  private Page page;
}
