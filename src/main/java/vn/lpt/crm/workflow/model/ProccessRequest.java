/** */
package vn.lpt.crm.workflow.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/** @author HP */
@Getter
@Setter
public class ProccessRequest {

  private String objectId;
  private String objectModel;
  private String assignee;
  private List<String> teams;
  private String proccessName;
  private String lane;
  private String taskId;
  private String leadId;
  private String name;
  private String accNo;
  private String phoneMobile;
  private Map<String, Object> condition;
}
