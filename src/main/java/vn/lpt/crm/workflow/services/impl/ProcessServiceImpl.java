package vn.lpt.crm.workflow.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.payloads.StartProcessPayload;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import vn.lpt.crm.workflow.services.ProcessService;

import java.util.Map;

@Service("ProcessService")
public class ProcessServiceImpl implements ProcessService {
  private static final Logger LOGGER = LoggerFactory.getLogger(ProcessServiceImpl.class);

  private final ProcessRuntime processRuntime;
  private final RuntimeService runtimeService;

  private ObjectMapper mapper = new ObjectMapper();

  public ProcessServiceImpl(RuntimeService runtimeService, ProcessRuntime processRuntime) {
    this.processRuntime = processRuntime;
    this.runtimeService = runtimeService;
  }

  @Override
  public ProcessInstance startProcess(StartProcessPayload payload) {
    return processRuntime.start(payload);
  }

  public void callTask(Map<String, Object> data) {
    System.out.println("đã vào ");
  }

  public void execute(DelegateExecution execution) throws Exception {
    System.out.println(execution.getId());
    System.out.println(execution.getVariable("data"));
    System.out.println("Hello from My First Spring Bean");
  }
}
