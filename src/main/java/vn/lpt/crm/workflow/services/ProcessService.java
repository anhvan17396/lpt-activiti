package vn.lpt.crm.workflow.services;

import org.activiti.api.process.model.ProcessDefinition;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.payloads.StartProcessPayload;
import org.activiti.api.runtime.shared.query.Page;

public interface ProcessService {
  ProcessInstance startProcess(StartProcessPayload payload);
}
